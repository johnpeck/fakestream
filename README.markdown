# fakestream #

Periodically updates a file with a new timestamped data point.  This simulates real-time sensor data streamed to a file.  Timestamps are as shown below...

```console
john:~/fakestream/src $ tail -f fakestream_archive.csv
2019-07-05 15:19:36.829, 0.6706856120935185
2019-07-05 15:19:37.831, 0.10598633965186258
2019-07-05 15:19:38.832, -0.4992743142248364
2019-07-05 15:19:39.832, -0.9134602367411301
2019-07-05 15:19:40.834, -0.9780604105981402
```

...where `fakestream_archive.csv` is the default output file.  Timestamps have millisecond precision.  

## Getting Started ##

### Try the single-file executables ###

The fastest way to try this is to download the single-file executable for your platform.  I've put releases for Windows and Linux on [the downloads page](https://bitbucket.org/johnpeck/fakestream/downloads/).  These will simply start up and write a sample from a 0.1 Hz sine wave
to a `fakestream_archive.csv` file every 100 milliseconds.  

If you're starting one of these executables from a command line, you can change the sample period with the `-t` flag.  Use the `-h` flag to see command line options.

### Download the source and set up Tcl ###

This code is written in Tcl, and uses the Tk toolkit.  An easy way to run from source is to install **ActiveTcl** from [ActiveState](https://www.activestate.com/products/activetcl/downloads/).  You'll then be able to run things with

```console
john:~/fakestream/src $ tclsh fakestream.tcl
```
