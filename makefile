# The name of the program or executable -- no extensions
progname = fakestream

# TCL file containing the main entry point
main_file = fakestream.tcl

# The version code.  This is where the code should be set.  This value
# will be written to the source files and show up in the filename.
# I'll use semantic versioning, so that each version should have
# major, minor, and patch codes.
revcode := 1.0.0

# Path to the TCL files making up the source
source_path = src

# Path to starkits and starpacks
executable_path = bin

# Local module path
#
# This is used for modules outside of tcllib.  For example tzint needs
# to be built manually.
local_module_path = lib

# List of modules from tcllib to copy over into the starkit.
tcllib_modules = cmdline

# 32-bit windows
#
# Download tclkits from:
#   http://www.tcl3d.org/html/appTclkits.html
#   ...I don't remember how to change the icon file
windows_x86_32_kit = "wraptools/tclkit-8.6.3-win32-ix86.exe"

# 64-bit linux
#
# Download tclkits from:
#   http://www.tcl3d.org/html/appTclkits.html
linux_x86_64_kit = "wraptools/tclkit-8.6.4-linux-amd64"

# Download Starkit Developer Extension from
# https://chiselapp.com/user/aspect/repository/sdx/index
sdx = "wraptools/sdx-20110317.kit"

#------------------------- Done with configuration ---------------------

source_files := $(wildcard $(source_path)/*.tcl)

# Source files will be relocated in the starkit virtual file system
vfs_files = $(addprefix $(progname).vfs/lib/app-$(progname)/, $(notdir $(source_files)))

help:
	@echo 'Makefile for $(progname) on $(platform)               '
	@echo '                                                      '
	@echo 'Usage:                                                '
	@echo '   make starkit                                       '
	@echo '       Make starkit                                   '
	@echo '   make win32                                         '
	@echo '       Make win32                                     '
	@echo '   make lin64                                         '
	@echo '       Make linux-amd64                               '                            
	@echo '------------------------------------------------------'
	@echo '   make clean                                         '
	@echo '       Clean up temporary files                       '


# I develop on a few different machines, with different operating
# systems.
#
# What platform are we on?
uname_value = "$(shell uname)"
ifeq ($(uname_value),"Linux")
	# Platform is Linux
	platform = "linux"
endif

ifeq ($(platform),"linux")
	tclsh = tclsh

	# Path for tcl modules to be copied into the starkit.
	# Download the zip archive of tcllib from core.tcl.tk.  Unzip
	# it and run the installer program in the root directory.
	tcllib_path = "/usr/lib/tcllib1.19"
endif

tcllib_module_directories = $(addprefix $(tcllib_path)/,$(tcllib_modules))

debug:
	@echo 'VFS files are: $(vfs_files)'
	@echo "Source files are: $(source_files)"
	@echo "tcllib path is: $(tcllib_path)"
	@echo "External modules are: $(tcllib_module_directories)"

# Make the starkit.
.PHONY: starkit
starkit: $(executable_path) $(executable_path)/$(progname).kit
$(executable_path)/$(progname).kit: $(source_files) \
                                    $(progname).vfs \
                                    $(progname).vfs/main.tcl \
                                    $(progname).vfs/lib \
                                    $(progname).vfs/lib/app-$(progname) \
                                    $(vfs_files)
	@echo 'Making starkit'
	$(tclsh) $(sdx) wrap $(progname)
	mv $(progname) $@

$(executable_path):
	mkdir $@

$(progname).vfs:
	mkdir $@

# Now source all the tcl code into the top of the vfs tree.  Note that
# you also have to replace each 'source' line from your tcl files with
# a command to source a file relative to the top of the vfs.
# Otherwise, tcl has no idea where these files are.
$(progname).vfs/main.tcl: $(progname).vfs
	echo 'package require starkit' > $@
	echo 'if {[starkit::startup] ne "sourced"} {' >> $@
	echo '    source [file join $$starkit::topdir'\
             'lib/app-$(progname)/$(main_file)]' >> $@
	echo '}' >> $@

# Creating the lib directory also copies all the needed modules.
$(progname).vfs/lib: $(progname).vfs
	mkdir $@
	cp -R $(tcllib_module_directories) $@
	chmod -R a+rx *

# Copy supporting files (like sourced tcl files) into the
# /lib/app-$(progname) directory along with $(progname).tcl
$(progname).vfs/lib/app-$(progname): $(progname).vfs \
                                     $(progname).vfs/lib
	mkdir $@

# $(vfs_files): $(source_files)
# 	cp $(source_files) $(progname).vfs/lib/app-$(progname)

# $(progname).vfs/lib/app-$(progname)/%: src/%
# 	cp $< $@

# Fix file path strings for starkits:
# Was:
#   source something.tcl
# Becomes:
#   source [file join $starkit::topdir lib/app-bitdecode/something.tcl]
#
# Was:
#   set wmiconfile icons/calc_16x16.png
# Becomes:
#   set wmiconfile [file join $starkit::topdir lib/app-bitdecode/icons/calc_16x16.png]
starkit_joinpath := [file join $$starkit::topdir lib/app-$(progname)/&]

# Copy source files into the starkit, filtering them to create proper
# location references inside the virtual file system.
#
# 1. Create proper path references for tcl source files.
# 2. Create proper path references for png icon files.
# 3. Create proper path references for tcl module (.tm) files
# 4. Create proper path references for test log (.testdata) files
# 5. Set the revision code to the setting in this makefile
$(progname).vfs/lib/app-$(progname)/%: $(source_path)/%
	sed 's,[[:graph:]]*\.tcl,$(starkit_joinpath),g'< $< | \
	  sed 's,[[:graph:]]*\.png,$(starkit_joinpath),g' | \
          sed 's,[[:graph:]]*\.tm,$(starkit_joinpath),g' | \
          sed 's,[[:graph:]]*\.testdata,$(starkit_joinpath),g' | \
          sed 's/set revcode.*/set revcode $(revcode)/g' > $@

# The starpack is the same as a starkit with a built-in tclkit.  The
# wrap command is the only difference.
.PHONY: win32
win32: bin/$(progname)-$(revcode)-win32-ix86.exe
bin/$(progname)-$(revcode)-win32-ix86.exe: starkit
	mkdir -p $(dir $@)
	$(tclsh) $(sdx) wrap $(progname) -runtime $(windows_x86_32_kit)
	mv $(progname) $@

.PHONY: lin64
lin64: bin/$(progname)-$(revcode)-linux-amd64
bin/$(progname)-$(revcode)-linux-amd64: starkit
	mkdir -p $(dir $@)
	$(tclsh) $(sdx) wrap $(progname) -runtime $(linux_x86_64_kit)
	mv $(progname) $@

.PHONY: clean
clean:
	rm -rf $(progname).vfs
	rm -f $(progname).bat
	rm -f *.log
	rm -f $(progname).cfg
	rm -f $(progname).kit
	rm -rf testrun

